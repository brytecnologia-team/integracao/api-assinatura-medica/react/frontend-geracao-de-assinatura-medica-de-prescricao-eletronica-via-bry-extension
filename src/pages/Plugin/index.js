import React, { useState, useEffect } from "react";
import Select from 'react-select';
import axios from 'axios';
import * as utils from "../../utils/utils.js";

export default function Plugin() {

  // CRIA TODAS AS VARIAVEIS QUE SERÃO ENVIADAS NA REQUISIÇÃO
  const [documento, setDocumento] = useState("");
  const [certificados, setCertificados] = useState("");
  const [certificadoSelecionado, setCertificadoSelecionado] = useState("");
  const [algoritmoHash, setAlgoritmoHash] = useState("SHA1");
  const [tipoDocumento, setTipoDocumento] = useState("");
  const [tipoProfissional, setTipoProfissional] = useState("");
  const [numero, setNumero] = useState("");
  var   [numeroOID] = useState("");
  const [UF, setUF] = useState("");
  var   [UFOID] = useState("");
  const [especialidade, setEspecialidade] = useState("");
  var   [especialidadeOID] = useState("");
  const [perfil, setPerfil] = useState("BASICA");
  const [assinaturaVisivel, setAssinaturaVisivel] = useState("false");
  const [incluirIMG, setIncluirIMG] = useState("false");
  const [imagem, setImagem] = useState("");
  const nonces = "aslkdnjalskdnjakld";
  const [altura, setAltura] = useState("");
  const [largura, setLargura] = useState("");
  const [coordenadaX, setCoordenadaX] = useState("");
  const [coordenadaY, setCoordenadaY] = useState("");
  const [posicao, setPosicao] = useState("INFERIOR_ESQUERDO");
  const [pagina, setPagina] = useState("PRIMEIRA");
  const [texto, setTexto] = useState("");
  const [incluirCN, setIncluirCN] = useState("false");
  const [incluirCPF, setIncluirCPF] = useState("false");
  const [incluirEmail, setIncluirEmail] = useState("false");
  const [incluirTXT, setIncluirTXT] = useState("false");
  const [loading, setLoading] = useState(false);
  const extensaoInstalada = utils.isExtensionInstalled();
  const browser = utils.detectBrowser();

  // MÉTODO QUE EXECUTA QUANDO A PAGINA É RENDERIZADA.
  useEffect(() => {
    // SE A EXTENSAO TIVER INSTALADA, PEGA OS CERTIFICADOS INSTALADOS DO CLIENTE
    if (extensaoInstalada) {
      window.BryExtension.listCertificates().then((certificados) => {
        certificados.forEach(certificado => {
          certificado.label = certificado.name;
        });
        setCertificados(certificados);
      })
    }
  }, [extensaoInstalada])

  // FUNCÃO QUE SERÁ EXECUTADA QUANDO FOR CLICADO NO BOTÃO "Assinar"
    async function handleSubmit(event) {

        // PREVINE AÇÕES PADÕRES DE UMA PAGINA REACT
        event.preventDefault();
        // ALTERA A VARIÁVEL LOADING PRA TRUE PARA QUE APAREÇA MENSAGEM DE "Realizando assinatura do documento"
        setLoading(true);
        // CRIA O FORMDATA QUE SERÁ ENVIADO NA REQUISIÇÃO
        const data = new FormData();
        // INCLUIR AS VARIÁVEIS NO FORMDATA
        // CERTIFICADO DIGITAL QUE SERÁ USADO NA ASSINATURA
        data.append("certificado", certificadoSelecionado.certificateData);
        // PERFIL DE ASSINATURA (BASICA OU CARIMBO)
        data.append("perfil", perfil);
        // ALGORITMO HASH QUE SERÁ USADO NA CODIFICAÇÃO DO DOCUMENTO
        data.append("algoritmoHash", algoritmoHash);
        // DOCUMENTO NO FORMATO PDF QUE SERÁ ASSINADO
        data.append("documento", documento);
        // SE A ASSINATURA SERÁ VISIVEL NO DOCUMENTO
        data.append("assinaturaVisivel", assinaturaVisivel)

        if (assinaturaVisivel === "true") {
            // ALTURA DO CAMPO DE ASSINATURA
            data.append("altura", altura);
            // LARGURA DO CAMPO DE ASSINATURA
            data.append("largura", largura);
            // COORDENADA NO EIXO X ONDE O CAMPO DE ASSINATURA SERÁ POSICIONADO
            data.append("coordenadaX", coordenadaX);
            // COORDENADA NO EIXO Y ONDE O CAMPO DE ASSINATURA SERÁ POSICIONADO
            data.append("coordenadaY", coordenadaY);
            // POSIÇÃO DO CAMPO DE ASSINATURA (INFERIOR_ESQUERDO, INFERIOR_DIREITO, SUPERIOR_DIREITO, SUPERIOR_ESQUERDO)
            data.append("posicao", posicao);
            // PAGINA DA ASSINATURA
            data.append("pagina", pagina);
            // SE SERÁ INCLUSO OU NÃO O TEXTO
            data.append("incluirTXT", incluirTXT);

            if (incluirTXT === "true") {
                // TEXTO DE ASSINATURA
                data.append("texto", texto);
            }
            // SE SERÁ INCLUSO OU NÃO A IMAGEM
            data.append("incluirIMG", incluirIMG);

            if (incluirIMG === "true") {
                // IMAGEM QUE SERÁ INSERIDA NA ASSINATURA. ACEITA PNG, JPEG E BMP.
                data.append("imagem", imagem);
            }
            // SE SERÁ INCLUSO CPF NA ASSINATURA
            data.append("incluirCPF", incluirCPF);
            // SE SERÁ INCLUSO O NOME DO ASSINANTE NA ASSINATURA (COMMON NAME)
            data.append("incluirCN", incluirCN);
            // SE SERÁ INCLUSO O EMAIL DO ASSINANTE NA ASSINATURA
            data.append("incluirEmail", incluirEmail);
        }
        //ATRIBUI OS OIDs RESPECTIVOS AO TIPO DE MÉDICO
        if (tipoProfissional === "MEDICO") {
            numeroOID = "2.16.76.1.4.2.2.1";
            UFOID = "2.16.76.1.4.2.2.2";
            especialidadeOID = "2.16.76.1.4.2.2.3";
        } else if (tipoProfissional === "FARMACEUTICO") {
            numeroOID = "2.16.76.1.4.2.3.1";
            UFOID = "2.16.76.1.4.2.3.2";
            especialidadeOID = "2.16.76.1.4.2.3.3";
        } else {
            numeroOID = "2.16.76.1.4.2.12.1";
            UFOID = "2.16.76.1.4.2.12.2";
            especialidadeOID = "2.16.76.1.4.2.12.2";
        }
        data.append("tipoDocumento", tipoDocumento);
        data.append("tipoProfissional", tipoProfissional);
        data.append("numero", numero);
        data.append("numeroOID", numeroOID);
        data.append("UF", UF);
        data.append("UFOID", UFOID);
        data.append("especialidade", especialidade);
        data.append("especialidadeOID", especialidadeOID);

        if (tipoProfissional === '' || tipoDocumento === '') {
            window.alert("Conferir os dados inseridos")
            console.log("Conferir os dados inseridos")
            setLoading(false);
        } else {
            // FAZ A REQUISIÇÃO DE INICIALIZAÇÃO PARA O BACKEND
            try {
                const response = await axios.post("http://localhost:3333/assinador/inicializar", data);
                console.log(response.data)
                // ALTERA O JSON QUE RETORNOU DA INICIALIZAÇÃO PARA QUE TENHA OS PARAMETROS CORRETOS PARA A FUNÇÃO "sign" DA EXTENSÃO
                response.data.assinaturas = response.data.assinaturasInicializadas;
                response.data.assinaturas[0].hashes = response.data.assinaturas[0].messageDigest;

                response.data.assinaturas.forEach(assinatura => assinatura.hashes = [assinatura.hashes]);

                // // ASSINA O DOCUMENTO COM A EXTENSÃO USANDO A FUNÇÃO "sign"
                await window.BryExtension.sign(certificadoSelecionado.certId, JSON.stringify(response.data)).then(async (assinatura) => {
                    const extensiondata = new FormData();

                    extensiondata.append("cifrado", assinatura.assinaturas[0].hashes[0]);
                    extensiondata.append("insideNonce", nonces);
                    extensiondata.append("formatoDeDados", "Base64");
                    extensiondata.append("nonce", assinatura.nonce);

                    console.log(extensiondata)
                    // FAZ A REQUISIÇÃO DE FINALIZAÇÃO PARA O BACKEND 
                    const responseFin = await axios.post("http://localhost:3333/assinador/finalizar", extensiondata);
                    console.log(responseFin)
                    // FAZ DOWNLOAD DO ARQUIVO COM A HREF QUE VEM NA RESPOSTA DA REQUISIÇÃO
                    window.location.href = responseFin.data.documentos[0].links[0].href;

                }).catch(err => {
                    console.log("Erro ao cifrar os dados no BRy Extension: ");
                    alert(err);
                })
            } catch (err) {
                // CASO OCORRA ALGUM ERRO NA REQUISIÇÃO, MOSTRA UMA MENSAGEM PARA O USUÁRIO
                // window.alert("Erro ao assinar o documento: " + err.response.data.message)
                console.log(err)
            }
            setLoading(false);
        }
    }

  // HTML(JSX) DA PÁGINA  
  return (
    <>
      {/* SE A EXTENSÃO ESTIVER INSTALADA, RETORNA A PÁGINA COMUM, CASO NÃO, RETORNA UM PASSO A PASSO DE INSTALAÇÃO, DEPENDENDO DO BROWSER UTILIZADO */}
      {extensaoInstalada ? (
        <div>
          <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
          />
          <h2>Assinador BRy PDF (PLUGIN)</h2>
          {/* FORMULÁRIO PARA PEGAR OS DADOS DO USUÁRIO */}
          <form onSubmit={handleSubmit}>
            <label htmlFor="docLabel">Documento a ser assinado *</label>
            <label htmlFor="documento" className="fileUp">
              {documento ? (
                <React.Fragment>{documento.name}</React.Fragment>
              ) : (
                  <React.Fragment>
                    <i className="fa fa-upload"></i>
                  Selecione o arquivo
                  </React.Fragment>
                )}
              <input
                id="documento"
                type="file"
                accept=".pdf"
                required
                onChange={event => setDocumento(event.target.files[0])}
              />
            </label>

            <label htmlFor="certificado">Certificado Digital *</label>
            <Select
              id="udesc"
              options={certificados}
              onChange={event => setCertificadoSelecionado(event)}
              value={certificadoSelecionado} />

            {certificadoSelecionado ? (
              // QUANDO SELECIONA O CERTIFICADO, MOSTRA OS DADOS DO MESMO

              <React.Fragment>

                <label htmlFor="textarea">Emissor do Certificado:</label>
                <input type="text" rows="2" value={certificadoSelecionado.issuer} readOnly></input>

                <label htmlFor="textarea">Validade do Certificado:</label>
                <input type="text" rows="2" value={certificadoSelecionado.expirationDate} readOnly></input>

                <label htmlFor="textarea">Tipo do Certificado:</label>
                <input type="text" rows="2" value={certificadoSelecionado.certificateType} readOnly></input>

                <label htmlFor="textarea">Conteúdo do Certificado:</label>
                <textarea type="text" rows="7" value={certificadoSelecionado.certificateData} readOnly></textarea>

              </React.Fragment>
            ) : (
                ""
              )}

            <label htmlFor="perfil">Perfil de Assinatura *</label>
            <select
              name="perfil"
              value={perfil}
              onChange={event => setPerfil(event.target.value)}
            >
              <option value="BASICA">Basica</option>
              <option value="CARIMBO">Com carimbo do tempo</option>
            </select>

            <label htmlFor="algoritmoHash">Algoritmo Hash *</label>
            <select
              name="algoritmoHash"
              value={algoritmoHash}
              onChange={event => setAlgoritmoHash(event.target.value)}
            >
              <option value="SHA1">SHA1</option>
              <option value="SHA256">SHA256</option>
              <option value="SHA512">SHA512</option>
              </select>

                      <label htmlFor="tipoDocumento">Tipo de Documento *</label>
                      <select
                          name="tipoDocumento"
                          value={tipoDocumento}
                          onChange={event => setTipoDocumento(event.target.value)}
                      >
                          <option value="">Selecione uma opção...</option>
                          <option value="2.16.76.1.12.1.1">Prescrição de medicamento</option>
                          <option value="2.16.76.1.12.1.2">Atestado médico</option>
                          <option value="2.16.76.1.12.1.3">Solicitação de exame</option>
                          <option value="2.16.76.1.12.1.4">Laudo laboratorial</option>
                          <option value="2.16.76.1.12.1.5">Sumária de alta</option>
                          <option value="2.16.76.1.12.1.6">Registro de atendimento clinico</option>
                          <option value="2.16.76.1.12.1.7">Dispensação de medicamentos</option>
                          <option value="2.16.76.1.12.1.8">Vacinação</option>
                          <option value="2.16.76.1.12.1.11">Relatório médico</option>
                      </select>

                      <label htmlFor="tipoProfissional">Tipo de Profissional*</label>
                      <select
                          name="tipoProfissional"
                          value={tipoProfissional}
                          onChange={event => setTipoProfissional(event.target.value)}
                      >
                          <option value="">Selecione uma opção...</option>
                          <option value="MEDICO">Médico</option>
                          <option value="FARMACEUTICO">Farmacêutico</option>
                          <option value="ODONTOLOGISTA">Odontologista</option>
                      </select>

                      <label htmlFor="cpf">Número no respectivo conselho *</label>
                      <input
                          id="numero"
                          type="number"
                          required
                          placeholder="Número no Conselho"
                          value={numero}
                          onChange={event => setNumero(event.target.value)}
                      />

                      <label htmlFor="cpf">UF do Profissional *</label>
                      <input
                          id="UF"
                          type="text"
                          required
                          placeholder="Estado"
                          value={UF}
                          onChange={event => setUF(event.target.value)}
                      />

                      <label htmlFor="cpf">Especialidade *</label>
                      <input
                          id="especialidade"
                          type="text"
                          required
                          placeholder="Ex: Cardiologista"
                          value={especialidade}
                          onChange={event => setEspecialidade(event.target.value)}
                      />


            <label htmlFor="assinaturaVisivel">Assinatura visível? *</label>
            <select
              name="assinaturaVisivel"
              value={assinaturaVisivel}
              onChange={event => setAssinaturaVisivel(event.target.value)}
            >
              <option value="false" >Não</option>
              <option value="true" >Sim</option>
            </select>

            {assinaturaVisivel === "true" ? (
              <>
               <label htmlFor="altura">Altura do campo de assinaura *</label>
                <input
                  id="altura"
                  type="number"
                  required
                  placeholder="Altura do campo de assinaura"
                  value={altura}
                  onChange={event => setAltura(event.target.value)}
                />

                <label htmlFor="largura">Largura do campo de assinaura *</label>
                <input
                  id="largura"
                  type="number"
                  required
                  placeholder="Largura do campo de assinaura"
                  value={largura}
                  onChange={event => setLargura(event.target.value)}
                />

                <label htmlFor="coordenadaX">Coordenada X *</label>
                <input
                  id="coordenadaX"
                  type="number"
                  required
                  placeholder="Coordenada X do campo de assinaura"
                  value={coordenadaX}
                  onChange={event => setCoordenadaX(event.target.value)}
                />

                <label htmlFor="coordenadaY">Coordenada Y *</label>
                <input
                  id="coordenadaY"
                  type="number"
                  required
                  placeholder="Coordenada Y do campo de assinaura"
                  value={coordenadaY}
                  onChange={event => setCoordenadaY(event.target.value)}
                />

                <label htmlFor="posicao">Posicao do campo de assinaura *</label>
                <select
                  name="posicao"
                  value={posicao}
                  onChange={event => setPosicao(event.target.value)}
                >
                  <option value="INFERIOR_ESQUERDO">Inferior Esquerdo</option>
                  <option value="INFERIOR_DIREITO">Inferior Direito</option>
                  <option value="SUPERIOR_ESQUERDO">Superior Esquerdo</option>
                  <option value="SUPERIOR_DIREITO">Superior Direito</option>
                </select>

                <label htmlFor="PAGINA">Página do campo de assinaura *</label>
                <select
                  name="pagina"
                  value={pagina}
                  onChange={event => setPagina(event.target.value)}
                >
                  <option value="PRIMEIRA">Primeira</option>
                  <option value="ULTIMA">Última</option>
                  <option value="TODAS">Todas</option>
                </select>

                <label htmlFor="incluirIMG">Incluir imagem? *</label>
            <select
              name="incluirIMG"
              value={incluirIMG}
              onChange={event => setIncluirIMG(event.target.value)}
            >
              <option value="false" >Não</option>
              <option value="true" >Sim</option>
            </select>

            {incluirIMG === "true" ? (
              <>
                <h2>Configurações de imagem</h2>

                <label htmlFor="lblImagem">Imagem *</label>

                <label htmlFor="imagem" className="fileUp">
                  {imagem ? (
                    <React.Fragment>{imagem.name}</React.Fragment>
                  ) : (
                      <React.Fragment>
                        <i className="fa fa-upload"></i>
                      Selecione a imagem de assinatura
                      </React.Fragment>
                    )}

                  <input
                    id="imagem"
                    type="file"
                    accept=".png, .jpeg, .jpg, .bmp"
                    required
                    onChange={event => setImagem(event.target.files[0])}
                  />
                </label>
               
              </>
            ) : ("")}
                <label htmlFor="incluirTXT">Incluir texto? *</label>
                <select
                  name="incluirTXT"
                  value={incluirTXT}
                  onChange={event => setIncluirTXT(event.target.value)}
                >
                  <option value="false" >Não</option>
                  <option value="true" >Sim</option>
                </select>

                {incluirTXT === "true" ? (
                  <>
                    <label htmlFor="texto">Texto de Assinatura*</label>
                    <input
                      id="texto"
                      type="text"
                      required
                      placeholder="Texto incluso na Assinatura"
                      value={texto}
                      onChange={event => setTexto(event.target.value)}
                      />

                  </>
                ) : (
                  ""
                )}

                <label htmlFor="incluirCPF">Incluir CPF na Assinatura?</label>
                <select
                  name="incluirCPF"
                  value={incluirCPF}
                  onChange={event => setIncluirCPF(event.target.value)}
                >
                  <option value="false" >Não</option>
                  <option value="true" >Sim</option>
                </select>

                <label htmlFor="incluirEmail">Incluir Email na Assinatura?</label>
                <select
                  name="incluirEmail"
                  value={incluirEmail}
                  onChange={event => setIncluirEmail(event.target.value)}
                >
                  <option value="false" >Não</option>
                  <option value="true" >Sim</option>
                </select>

                <label htmlFor="incluirCN">Incluir Nome na Assinatura?</label>
                <select
                  name="incluirCN"
                  value={incluirCN}
                  onChange={event => setIncluirCN(event.target.value)}
                >
                  <option value="false" >Não</option>
                  <option value="true" >Sim</option>
                </select>
                  </>
                ) : ("")}

                <button className="btn" type="submit">
                  Assinar
                </button>

                <label>
                  {loading ? "Realizando a assinatura do documento..." : ""}
                </label>
              </form>
            </div>
          ) : (
              <div className="extension">
                {browser === "chrome" ? (
                  <div className="isChrome">
                    <h2 className="extension-message">
                      Detectamos que a extensão para Assinatura Digital não está
                      instalada!
                  </h2>
                    <h4>
                      Segue abaixo um passo a passo para instalação da extensão:
                  </h4>
                    <p>
                      <strong>1º Passo -</strong> Clique no botão abaixo para acessar
                    a Extensão no Chrome WebStore
                  </p>
                    <a
                      href="https://chrome.google.com/webstore/detail/dhikfimimcjpoaliefjlffaebdeomeni"
                      className="btn btn-lg btn-primary btn-extension-install"
                    >
                      Instalar Extensão via Chrome WebStore!
                  </a>

                    <p>
                      <strong>2º Passo -</strong> Clique no botão{" "}
                      <strong>USAR NO CHROME</strong>
                    </p>
                    <img
                      className="imgUsar"
                      alt="Print use on chrome button"
                      src={require("../../assets/imgs/use_on_chrome_button.jpg")}
                    />
                    <p>
                      <strong>3º Passo -</strong> Você deve retornar para esta página
                    que ela será atualizada.
                  </p>
                  </div>
                ) : (
                    ""
                  )}
                {browser === "firefox" ? (
                  <div className="isFirefox">
                    <h2 className="extension-message">
                      Detectamos que a extensão para Assinatura Digital não está
                      instalada!
                  </h2>

                    <p>
                      <strong>1º Passo -</strong> Clique no botão abaixo para instalar
                    a extensão
                  </p>

                    <a
                      href="https://www.bry.com.br/downloads/extension/firefox/assinatura_digital_bry.xpi"
                      className="btn btn-lg btn-primary btn-extension-install"
                    >
                      Instalar Extensão!
                  </a>

                    <p>
                      <strong>2º Passo -</strong> Atualize a página
                  </p>
              </div>
            ) : (
                ""
              )}
            {browser === "edge" ? (
              <div className="isEdge">
                <h2 className="extension-message">
                  Lamentamos, mas uma versão da extensão só estará disponível para
                  o seu navegador na próxima atualização!
              </h2>
              </div>
            ) : (
                ""
              )}
            {browser === "opera" ? (
              <div className="isOpera">
                <h2 className="extension-message">
                  Lamentamos, mas uma versão da extensão só estará disponível para
                  o seu navegador na próxima atualização!
              </h2>
              </div>
            ) : (
                ""
              )}

            {browser === "safari" ? (
              <div className="isSafari">
                <h2>
                  Detectamos que a Extensão para Assinatura Digital não está
                  instalanda
              </h2>
                <input
                  type="image"
                  width="250"
                  src={require("../../assets/imgs/baixar.png")}
                  alt="Logo apple store"
                />
                <h4>
                  Após a instalação é necessário habilitar a extensão nas
                  preferências do Safari.
              </h4>
                <p>
                  <b>1º Abra o aplicativo "BRy Assinatura Digital" instalado</b>
                </p>
                <p>
                  <strong>
                    2º Dentro do aplicativo, clique em habilitar a extensão nas
                    preferências do Safari
                </strong>
                </p>
                <img
                  src={require("../../assets/imgs/app.png")}
                  alt="Print button extension"
                />
                <p>
                  Caso a opção para habilitar a extensão não apareça nas
                  preferências do Safari, encerre o navagador, e repita o passo 2.
              </p>
                <p>
                  <strong>
                    4º Após ativar a extensão, basta recarregar a página.
                </strong>
                </p>
                <button onClick={document.location.reload(true)}>
                  Recarregar página
              </button>
              </div>
            ) : (
                ""
              )}
            {browser === "iE" ? (
              <div className="isIE">
                <div className="extension-message">
                  <h2>
                    Lamentamos, mas uma versão da extensão só estará disponível
                    para o seu navegador na próxima atualização!
                </h2>
                </div>
              </div>
            ) : (
                ""
              )}
            {browser === "unknown" ? (
              <div className="isUnknown">
                <h2 className="extension-message">
                  Não foi possível identificar o seu browser!
              </h2>
              </div>
            ) : (
                ""
              )}
          </div>
        )}
    </>
  );
}
